import {Component, OnInit} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend';
  baseUrl = environment.baseUrl;
  user = {
    first_name: '',
    last_name: ''
  };
  users = [];
  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.getAllUser();
  }
  getAllUser() {
    this.http.get<any[]>(this.baseUrl).subscribe(res => {
      this.users = res;
    },
      error => {
        alert('Failed to make API call');
      });
  }
  createUser() {
    if (this.user.first_name && this.user.last_name) {
      this.http.post(this.baseUrl, this.user).subscribe(res => {
          this.getAllUser();
      },
        error => {
          alert('Failed to make API call');
        });
    }
  }
}
