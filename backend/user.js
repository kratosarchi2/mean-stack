const mongoose = require('mongoose');
const UserSchema = mongoose.Schema({
    first_name: {
        type: String,
        require: true,
    },
    last_name: {
        type: String,
        require: true
    }}, { timestamps: true });

module.exports = User = mongoose.model('User', UserSchema);
