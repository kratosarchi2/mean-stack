const express = require('express')
const bodyParser= require('body-parser')
const mongoose = require('mongoose');
const cors = require('cors');
const User = require('./user');
const app = express()
const port = 3000

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.get('/', (req, res) => {
    User.find()
    .then(data => {
      res.status(200).send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });

})
app.post('/', (req, res) => {
    if (req.body.first_name && req.body.last_name ) {
        const user = new User({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
          });
          user.save(user)
            .then(data => {
            res.status(200).send(data);
            })
            .catch(err => {
            res.status(500).send({
                message:
                err.message || "Some error occurred while creating the User."
            });
        });
        }
      else {
        res.status(400).send({ message: "Fields required, cannot be empty" });
        return;
      }

})

mongoose.connect('mongodb://mongo-db-dev:27017/plaisir-du-jardin', {
    useNewUrlParser: true, useUnifiedTopology: true
}, () => console.log('Successfully connected to DB'));


app.listen(port, () => {
  console.log(`Dev app listening at http://localhost:${port}`)
})